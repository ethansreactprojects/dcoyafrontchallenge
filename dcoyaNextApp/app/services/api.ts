import Definitions from "../base/definitions";
import Country from "../models/country";

export default class API {
  static async getAllCountries(): Promise<Map<string, Country> | string> {
    const res = await fetch(`https://countryapi.io/api/all`, {
      method: "get",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${Definitions.countryApiKey}`,
      },
    });
    const data = await res.json();
    if (res.ok) {
      let map = new Map<string, Country>();
      for (const key in data) {
        if (Object.prototype.hasOwnProperty.call(data, key)) {
          const value = data[key];
          //console.log(`${key}: ${value}`);
          let obj = Country.fromJSON(value);
          map.set(obj.name.toLowerCase(), obj);
        }
      }
      return map;
    }
    else{
      return data.error;
    }
  }
}
