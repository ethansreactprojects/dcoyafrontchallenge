import Country from "../models/country";
import xss from "xss";

export default class Helper {
  public static epochTimeNow(): number {
    return new Date().getTime();
  }

  public static mapToObject(map: Map<string, Country>): {
    [key: string]: Country;
  } {
    const obj: Record<string, Country> = {};
    map.forEach((value, key) => {
      obj[key] = value;
    });
    return obj;
  }

  public static objectToMap(obj: { [key: string]: Country }) {
    const map = new Map<string, Country>();
    for (const [key, value] of Object.entries(obj)) {
      map.set(key, value);
    }
    return map;
  }

  public static logOperation(operation: string) {
    console.log(
      `----------------------\n` +
        `Application LOG: ${new Date().toUTCString()}\n${operation}\n` +
        `----------------------\n`
    );
  }

  public static encodeUserString(userString: string): string {
    const sanitizedString = xss(userString);
    return sanitizedString;
  }
}
