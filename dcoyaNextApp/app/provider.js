"use client";

import { Provider } from "react-redux";
import { store } from "./store/store";
import { enableMapSet } from 'immer';

// Enable the Immer plugin for Map and Set
enableMapSet();

export function Providers({ children }) {
  return <Provider store={store} style={{height:"100%"}}>{children}</Provider>;
}
