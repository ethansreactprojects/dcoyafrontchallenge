import MainPage from "./componenets/mainPage";
import { Providers } from "./provider";



function Home() {
  return (
    <main className="flex flex-col h-screen overflow-hidden">
      <div className="max-w-full max-h-full overflow-y-auto overflow-x-hidden" style={{height:"100%"}}>
      <Providers>
        <MainPage />
      </Providers>
      </div>
    </main>
  );
}

export default Home;
