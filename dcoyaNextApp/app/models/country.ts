import { fromJSON } from "postcss";

export class Currency {
  public name: string;
  public symbol: string;

  public constructor(name: string, symbol: string) {
    this.name = name;
    this.symbol = symbol;
  }

  public static fromJSON(json: any) {
    return new Currency(json.name, json.symbol);
  }
}

export default class Country {
  public name: string;
  public officialName: string;
  public topLevelDomain: Array<string>;
  public alpha2Code: string;
  public callingCode: string;
  public capital: string;
  public altSpellings: Array<string>;
  public region: string;
  public population: number;
  public latLngCountry: Array<number>;
  public latLngCapital: Array<number>;
  public area: number;
  public timezones: Array<string>;
  public currencies: Map<string, Currency>;
  public flagLarge: string;

  public constructor(
    name: string,
    officialName: string,
    topLevelDomain: Array<string>,
    alpha2Code: string,
    callingCode: string,
    capital: string,
    altSpellings: Array<string>,
    region: string,
    population: number,
    latLngCountry: Array<number>,
    latLngCapital: Array<number>,
    area: number,
    timezones: Array<string>,
    currencies: Map<string, Currency>,
    flagLarge: string
  ) {
    this.name = name;
    this.officialName = officialName;
    this.topLevelDomain = topLevelDomain;
    this.alpha2Code = alpha2Code;
    this.callingCode = callingCode;
    this.capital = capital;
    this.altSpellings = altSpellings;
    this.region = region;
    this.population = population;
    this.latLngCountry = latLngCountry;
    this.latLngCapital = latLngCapital;
    this.area = area;
    this.timezones = timezones;
    this.currencies = currencies;
    this.flagLarge = flagLarge;
  }

  public static fromJSON(json: any) {
    let currenciesMap = new Map<string, Currency>();
    for (const key in json.currencies) {
      if (Object.prototype.hasOwnProperty.call(json.currencies, key)) {
        const value = json.currencies[key];
        //console.log(`${key}: ${value}`);
        currenciesMap.set(key, Currency.fromJSON(value));
      }
    }

    return new Country(
      json.name,
      json.official_name,
      json.topLevelDomain,
      json.alpha2Code,
      json.callingCode,
      json.capital,
      json.altSpellings,
      json.region,
      json.population,
      json.latLng.country,
      json.latLng.capital,
      json.area,
      json.timezones,
      currenciesMap,
      json.flag.large
    );
  }
}
