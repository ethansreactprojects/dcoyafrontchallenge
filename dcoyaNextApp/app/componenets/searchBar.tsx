import React from "react";
import { useState } from "react";
import SearchIcon from "@mui/icons-material/Search";
import { InputAdornment } from "@mui/material";
import { TextField } from "@mui/material";
import Helper from "../base/helper";

interface SearchBarProps {
  width: number;
  onChange: CallableFunction;
  placeholder: string;
  label: string;
  value: string; // if we want value to be controlled, default is uncontrolled
  type: string;
}

const SearchBar = ({ props }: { props: SearchBarProps }) => {
  const [value, setValue] = useState(props.value !== null ? props.value : "");
  return (
    <TextField
      variant="standard"
      focused
      placeholder={props.placeholder}
      value={value}
      label={props.label}
      type={props.type}
      onChange={(e) => {
        let sanitized = Helper.encodeUserString(e.target.value);
        setValue(sanitized);
        props.onChange(sanitized);
      }}
      sx={{ width: props.width }}
      InputProps={{
        endAdornment: (
          <InputAdornment position="start">
            <SearchIcon color="primary" />
          </InputAdornment>
        ),
      }}
    />
  );
};

export default SearchBar;
