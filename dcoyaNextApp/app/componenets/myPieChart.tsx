import React, { PureComponent, useEffect, useState } from "react";
import {
  PieChart,
  Pie,
  Legend,
  Tooltip,
  ResponsiveContainer,
  Cell,
} from "recharts";
import Country from "../models/country";
import { Box, Paper, Typography } from "@mui/material";

function generateData(
  data: Map<string, Country>,
  property: keyof Country
): any[] {
  let arr: any[] = [];
  data.forEach((val, key) => {
    arr.push({ name: val.name, value: val[property] });
  });
  return arr;
}

const COLORS = [
  "#b30000",
  "#7c1158",
  "#4421af",
  "#1a53ff",
  "#0d88e6",
  "#00b7c7",
  "#5ad45a",
  "#8be04e",
  "#ebdc78" /*
  "#ea5545",
  "#f46a9b",
  "#ef9b20",
  "#edbf33",
  "#ede15b",
  "#bdcf32",
  "#87bc45",
  "#27aeef",
  "#b33dc6",*/,
];

function SpecificPieChart({
  data,
  isMobile,
}: {
  data: any[];
  isMobile: boolean;
}) {
  return (
    <ResponsiveContainer width="100%" height="60%">
      <PieChart>
        <Pie
          data={data}
          dataKey="value"
          nameKey="name"
          cx="50%"
          cy="50%"
          innerRadius={isMobile ? 15 : 30} // Adjust the inner radius to make the Pie larger
          outerRadius={isMobile ? 25 : 70} // Adjust the outer radius to make the Pie larger
          fill="#8884d8"
          label
        >
          {data.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>
        <Legend />
        <Tooltip />
      </PieChart>
    </ResponsiveContainer>
  );
}

export default function MyPieChart({
  currentData,
}: {
  currentData: Map<string, Country>;
}) {
  const [screenSize, setScreenSize] = useState(window.innerWidth);
  useEffect(() => {
    const handleResize = () => {
      setScreenSize(window.innerWidth);
    };

    // Add event listener for window resize
    window.addEventListener("resize", handleResize);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  let populationData = generateData(currentData, "population");
  let areaData = generateData(currentData, "area");
  const isMobile = screenSize <= 768;
  return (
    <Box
      display="flex"
      justifyContent="center"
      flexDirection={"column"}
      alignItems="center"
      mt={10}
      sx={{ height: "100%", width: "100%" }}
    >
      <Paper sx={{ height: "50%", width: "80%", mb: 0 }}>
        <Box display="flex" justifyContent="center" alignItems="center" mt={10}>
          <Typography variant="h5">Population Pie</Typography>
        </Box>
        <SpecificPieChart data={populationData} isMobile={isMobile} />
      </Paper>
      <Paper sx={{ height: "50%", width: "80%", mb: 0 }}>
        <Box display="flex" justifyContent="center" alignItems="center" mt={10}>
          <Typography variant="h5">Area Pie</Typography>
        </Box>
        <SpecificPieChart data={areaData} isMobile={isMobile} />
      </Paper>
      <Box mt={5}></Box>
    </Box>
  );
}
