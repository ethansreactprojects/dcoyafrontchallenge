import * as React from "react";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import RefreshIcon from "@mui/icons-material/Refresh";
import { visuallyHidden } from "@mui/utils";
import Country, { Currency } from "../models/country";
import SearchBar from "./searchBar";
import { Container, ThemeProvider, createTheme } from "@mui/material";

export interface Data {
  name: string;
  callingCode: string;
  capital: string;
  region: string;
  population: number;
  area: number;
  flagLarge: string;
  currency: string;
}

function createData(
  name: string,
  callingCode: string,
  capital: string,
  region: string,
  population: number,
  area: number,
  flagLarge: string,
  currency: string
): Data {
  return {
    name,
    callingCode,
    capital,
    region,
    population,
    area,
    flagLarge,
    currency,
  };
}

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = "asc" | "desc";

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key
): (
  a: { [key in Key]: number | string },
  b: { [key in Key]: number | string }
) => number {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// Since 2020 all major browsers ensure sort stability with Array.prototype.sort().
// stableSort() brings sort stability to non-modern browsers (notably IE11). If you
// only support modern browsers you can replace stableSort(exampleArray, exampleComparator)
// with exampleArray.slice().sort(exampleComparator)
function stableSort<T>(
  array: readonly T[],
  comparator: (a: T, b: T) => number
) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
}

const headCells: readonly HeadCell[] = [
  {
    id: "name",
    numeric: false,
    disablePadding: true,
    label: "Name (and general information)",
  },
  {
    id: "callingCode",
    numeric: false,
    disablePadding: false,
    label: "Calling Code",
  },
  {
    id: "currency",
    numeric: false,
    disablePadding: false,
    label: "Currency",
  },
  {
    id: "capital",
    numeric: false,
    disablePadding: false,
    label: "Capital",
  },
  {
    id: "region",
    numeric: false,
    disablePadding: false,
    label: "Region",
  },
  {
    id: "population",
    numeric: true,
    disablePadding: false,
    label: "Population Size",
  },
  {
    id: "area",
    numeric: true,
    disablePadding: false,
    label: "Area (km²)",
  },
  {
    id: "flagLarge",
    numeric: false,
    disablePadding: false,
    label: "Flag",
  },
];

interface EnhancedTableProps {
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler =
    (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox"></TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

function EnhancedTableToolbar({
  onChange,
  onClear,
}: {
  onChange: CallableFunction;
  onClear: CallableFunction;
}) {
  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
      }}
    >
      {
        <Typography
          sx={{ flex: "1 1 100%" }}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          Countries
        </Typography>
      }
      {
        <SearchBar
          props={{
            width: 200,
            label: "Serach by name",
            value: "",
            onChange: (val: string) => {
              onChange(val);
            },
            placeholder: "Israel",
            type: "search",
          }}
        />
      }
      {
        <Tooltip title="Reset List">
          <IconButton
            onClick={() => {
              onClear();
            }}
          >
            <RefreshIcon />
          </IconButton>
        </Tooltip>
      }
    </Toolbar>
  );
}

export default function EnhancedTable({
  data,
  onSearchQuery,
  onClear,
  onRowsChange,
}: {
  data: Map<string, Country>;
  onSearchQuery: CallableFunction;
  onClear: CallableFunction;
  onRowsChange: CallableFunction;
}) {
  const [order, setOrder] = React.useState<Order>("asc");
  const [orderBy, setOrderBy] = React.useState<keyof Data>("name");
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(true);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [rows, setRows] = React.useState<Array<Data>>([]);
  const [myData, setData] = React.useState<Map<string, Country>>(data);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof Data
  ) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDense(event.target.checked);
  };

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const visibleRows = React.useMemo(
    () =>
      stableSort(rows, getComparator(order, orderBy)).slice(
        page * rowsPerPage,
        page * rowsPerPage + rowsPerPage
      ),
    [order, orderBy, page, rowsPerPage, rows]
  );

  const handleData = () => {
    var row = new Array<Data>();
    /*[
          createData("Israel", "+972", "jer", "asia", 123456, 35004, "test"),
        ];*/
    if (myData !== null && myData !== undefined) {
      myData.forEach((value, key) => {
        let curr =
          value!.currencies !== undefined
            ? Array.from(value!.currencies!).map(([key, val]) => {
                return `${key}(${val.symbol}) `;
              })
            : [""];
        let currencyStr = "";
        curr.forEach((element) => {
          currencyStr += element;
        });
        row.push(
          createData(
            value!.name,
            value!.callingCode,
            value!.capital,
            value!.region,
            value!.population,
            value!.area,
            value!.flagLarge,
            currencyStr
          )
        );
      });
    }
    setRows(row);
  };

  React.useEffect(() => {
    handleData();
    setPage(0);
  }, [myData]);
  
  React.useEffect(() => {
    if(myData.size !== data.size){
      setData(data);
    }
  }, [data]);

  React.useEffect(() => {
    onRowsChange(visibleRows);
  }, [visibleRows]);

  const theme = createTheme({
    components: {
      MuiSwitch: {
        styleOverrides: {
          switchBase: {
            // Controls default (unchecked) color for the thumb
            color: "#82878c",
          },
          colorPrimary: {
            "&.Mui-checked": {
              // Controls checked color for the thumb
              color: "#0373fc",
            },
          },
          track: {
            // Controls default (unchecked) color for the track
            opacity: 0.2,
            backgroundColor: "#a9acb0",
            ".Mui-checked.Mui-checked + &": {
              // Controls checked color for the track
              opacity: 0.7,
              backgroundColor: "#6c83a1",
            },
          },
        },
      },
    },
  });
  return (
    <ThemeProvider theme={theme}>
      <Box sx={{ width: "100%", overflowX: "auto" }}>
        <Container component={Paper} sx={{ width: "100%", mb: 0 }}>
          <TableContainer>
            <EnhancedTableToolbar onChange={onSearchQuery} onClear={onClear} />
            <Box display="flex" justifyContent="star" alignItems="start">
              <TablePagination
                rowsPerPageOptions={[5, 10, 15]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Box>
            <Table
              sx={{ minWidth: 750 }}
              aria-labelledby="tableTitle"
              size={dense ? "small" : "medium"}
            >
              <EnhancedTableHead
                order={order}
                orderBy={orderBy}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
              />
              <TableBody>
                {visibleRows === undefined
                  ? "loading"
                  : visibleRows.map((row, index) => {
                      const labelId = `enhanced-table-checkbox-${index}`;
                      let currCountry = myData.get(row.name.toLowerCase());
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          aria-checked={false}
                          tabIndex={-1}
                          key={row.name}
                          selected={false}
                          sx={{ cursor: "pointer" }}
                        >
                          <TableCell padding="checkbox"></TableCell>
                          <TableCell
                            component="th"
                            id={labelId}
                            scope="row"
                            padding="none"
                          >
                            <Typography
                              variant="h6"
                              id="tableTitle"
                              component="div"
                            >
                              {row.name}
                            </Typography>
                            {currCountry?.officialName},
                            {currCountry?.alpha2Code}
                            {/*currCountry?.topLevelDomain*/}
                          </TableCell>
                          <TableCell align="left">{row.callingCode}</TableCell>
                          <TableCell align="left">{row.currency}</TableCell>
                          <TableCell align="left">{row.capital}</TableCell>
                          <TableCell align="left">{row.region}</TableCell>
                          <TableCell align="right">{row.population}</TableCell>
                          <TableCell align="right">{row.area}</TableCell>
                          <TableCell align="left">
                            <Box
                              component="img"
                              sx={{
                                height: 100,
                                width: 150,
                                maxHeight: { xs: 15, md: 30 },
                                maxWidth: { xs: 30, md: 50 },
                              }}
                              alt={"flag of " + row.name}
                              src={row.flagLarge}
                            />
                          </TableCell>
                        </TableRow>
                      );
                    })}
                {emptyRows > 0 && (
                  <TableRow
                    style={{
                      height: (dense ? 33 : 53) * emptyRows,
                    }}
                  >
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <FormControlLabel
            sx={{ margin: 1 }}
            control={<Switch checked={dense} onChange={handleChangeDense} />}
            label="Dense padding"
          />
        </Container>
      </Box>
    </ThemeProvider>
  );
}
