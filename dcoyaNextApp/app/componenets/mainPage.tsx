"use client";
import { useDispatch, useSelector } from "react-redux";
import { loadData, reduceByName, clearReduce, setCurrentlyShownData } from "../store/slices/apiSlicer";
import { AppDispatch, RootState } from "../store/store";
import { useEffect } from "react";
import EnhancedTable, { Data } from "./muiSortableTable";
import { Box, Paper, Typography } from "@mui/material";
import Helper from "../base/helper";
import MyPieChart from "./myPieChart";

function MainPage() {
  const dispatch = useDispatch<AppDispatch>();
  const data = useSelector((state: RootState) => state.api.searchedDataSet);
  const currentShownData = useSelector((state: RootState) => state.api.currentlyShownData);
  const loaded = useSelector((state: RootState) => state.api.loaded);
  const error = useSelector((state: RootState) => state.api.error);
  const erroorMsg = useSelector((state: RootState) => state.api.errorMsg);

  useEffect(() => {
    dispatch(loadData());
  }, [dispatch]);
  /*useEffect(() => {
    console.log(loaded);
  }, [loaded]);*/

  if (!loaded) {
    return (
      <Box display="flex" justifyContent="center" alignItems="center">
        <Typography variant="h6" id="tableTitle" component="div">
          Loading...
        </Typography>
      </Box>
    );
  }

  if (error) {
    return <div>Error: {erroorMsg}</div>;
  }

  let myData = Helper.objectToMap(data);
  let currentDataShown = Helper.objectToMap(currentShownData);

  return (
    <div style={{ height: "100%" }}>
      <div style={{ height: "100%", margin: "10px" }}>
        <Box display="flex" justifyContent="center" alignItems="center">
          <Typography variant="h6" id="tableTitle" component="div">
            Country API
          </Typography>
        </Box>
        <EnhancedTable
          data={myData}
          onSearchQuery={(val: string) => {
            dispatch(reduceByName(val));
          }}
          onClear={() => {
            dispatch(clearReduce());
          }}
          onRowsChange={(data: Data[]) => {
            dispatch(setCurrentlyShownData(data));
          }}
        />
        <MyPieChart currentData={currentDataShown}/>
      </div>
    </div>
  );
}

export default MainPage;
