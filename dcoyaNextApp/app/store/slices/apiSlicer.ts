import Country from "@/app/models/country";
import API from "@/app/services/api";
import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { RootState } from "../store";
import Helper from "@/app/base/helper";
import { Data } from "@/app/componenets/muiSortableTable";

export class Types {
  public static readonly loadData = "loadData";
}

export interface ApiState {
  loaded: boolean;
  error: boolean;
  errorMsg: string;
  data: { [key: string]: Country };
  searchedDataSet: { [key: string]: Country };
  currentlyShownData: { [key: string]: Country };
}

const initialState: ApiState = {
  loaded: false,
  error: false,
  errorMsg: "",
  data: {},
  searchedDataSet: {},
  currentlyShownData: {},
};

export const loadData = createAsyncThunk<
  { [key: string]: Country } | any,
  void,
  {
    rejectValue: string | any;
    state: RootState;
  }
>(Types.loadData, async (_, thunkAPI) => {
  const state = thunkAPI.getState() as RootState;
  if (state.api.loaded) {
    return state.api.data;
  }
  const response = await API.getAllCountries();
  if (typeof response === "string") {
    return thunkAPI.rejectWithValue(response) as unknown as string;
  } else if (response instanceof Map) {
    //thunkAPI.fulfillWithValue(response);
    return Helper.mapToObject(response);
  }
});

const apiSlice = createSlice({
  name: "api",
  initialState,
  reducers: {
    reduceByName: (state, action: PayloadAction<string>) => {
      Helper.logOperation("reducing list by query:" + action.payload);
      if (state.loaded && !state.error) {
        let searchedMap = Helper.objectToMap(state.searchedDataSet);
        let initialMap = Helper.objectToMap(state.data);
        searchedMap.clear();
        let search = action.payload.toLowerCase();
        let found = initialMap.has(search);
        if (found) {
          searchedMap.set(search, initialMap.get(search)!);
        } else {
          initialMap.forEach((value, key) => {
            if (key.includes(search)) {
              searchedMap.set(key, value);
            }
          });
        }
        state.searchedDataSet = Helper.mapToObject(searchedMap);
      }
    },
    setCurrentlyShownData: (state, action: PayloadAction<Data[]>) => {
      Helper.logOperation("changing currently shown rows");
      if (state.loaded && !state.error) {
        let initialMap = Helper.objectToMap(state.data);
        let shownMap = new Map<string, Country>();
        action.payload.forEach((value, index) => {
          let key = value.name.toLowerCase();
          shownMap.set(key, initialMap.get(key)!);
        });
        state.currentlyShownData = Helper.mapToObject(shownMap);
      }
    },
    clearReduce: (state) => {
      Helper.logOperation("clearing filter");
      if (state.loaded && !state.error) {
        state.searchedDataSet = Helper.mapToObject(
          Helper.objectToMap(state.data)
        );
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadData.pending, (state) => {
        Helper.logOperation("Loading intial data");
        state.loaded = false;
        state.error = false;
      })
      .addCase(loadData.fulfilled, (state, action) => {
        state.loaded = true;
        state.error = false;
        state.data = action.payload;
        state.searchedDataSet = Helper.mapToObject(
          Helper.objectToMap(state.data)
        );
        Helper.logOperation("Initial data loaded");
      })
      .addCase(loadData.rejected, (state, action) => {
        state.loaded = true;
        state.error = true;
        state.errorMsg = action.payload;
        Helper.logOperation("error with intial data load");
      });
  },
});

export const { clearReduce, reduceByName, setCurrentlyShownData } =
  apiSlice.actions;
export default apiSlice.reducer;
