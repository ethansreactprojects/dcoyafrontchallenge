import { enableMapSet } from 'immer';
// Enable the Immer plugin for Map and Set
enableMapSet();

import { configureStore } from "@reduxjs/toolkit";
import apiSlicer from "./slices/apiSlicer";



export const store = configureStore({
  reducer: {
    api: apiSlicer,
  },
});


export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
