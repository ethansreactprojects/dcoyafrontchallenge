# dcoyaFrontChallenge

This project is an assignment given by Dcoya, and was made by Eitan Krimer.

## Usage

Clone the repository

run  `cd dcoyaNextApp`

run ```npm i```

run ```npm run dev```

visit ```http://localhost:3000``` (it might take a few moments to compile)

Filter the console using ```"Application LOG"``` to watch the logs.

Technologies used: React next.js(with .TS), Redux, MUI, Recharts, CountryAPI, XSS sanitizing.



